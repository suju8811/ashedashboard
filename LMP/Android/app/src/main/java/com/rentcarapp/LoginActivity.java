package com.rentcarapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.rentcarapp.http.ApiClient;

import java.util.HashMap;
import com.rentcarapp.MainActivity;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {

    private KProgressHUD hud;
    EditText etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etEmail.setText("loaner");
        etPassword.setText("welcome123");
        boolean isLogined = false;
        try{
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            isLogined = prefs.getBoolean("isLogined",false);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        if(isLogined) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            LoginActivity.this.finish();
        }

    }

    public void onClick(View v)
    {

        final String email = etEmail.getText().toString();
        final String pass = etPassword.getText().toString();
        if (email.isEmpty() || pass.isEmpty()){
            Toast.makeText(this,"Email or Password is incorrect",Toast.LENGTH_SHORT).show();
            return;
        }
        HashMap<String,String> map = new HashMap<>();
        map.put("userId",email);
        map.put("password",pass);
        ApiClient.ApiInterface apiInterface = ApiClient.getApiClient();
        Call<String> call = apiInterface.onTemplateRequestForResponseLogin(Constants.webservice_login,map);

        showProgressDialog();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("login", response.body());
                Toast.makeText(LoginActivity.this, response.body(), Toast.LENGTH_SHORT).show();
                if(response.body().contains("success"))
                {
                    try{
                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                        editor.putString("userId",email);
                        editor.commit();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                    try{
                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                        editor.putString("password",pass);
                        editor.commit();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                    try{
                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                        editor.putBoolean("isLogined",true);
                        editor.commit();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                } else {
                    Toast.makeText(LoginActivity.this,"Email or Password is incorrect",Toast.LENGTH_SHORT).show();
                }

                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(LoginActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("login", "fail"+t.getMessage());
            }
        });
    }




    public void showProgressDialog() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait...")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    private void hideProgressDialog() {
        hud.dismiss();
    }
}
