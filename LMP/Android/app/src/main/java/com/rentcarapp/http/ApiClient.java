// Decompiled by DJ v3.12.12.96 Copyright 2011 Atanas Neshkov  Date: 5/18/2015 5:57:30 PM
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 

package com.rentcarapp.http;


import com.rentcarapp.Constants;
import com.rentcarapp.LoginResponse;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public class ApiClient {

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Constants.base_endurl)
                    .addConverterFactory(GsonConverterFactory.create());
    private static int delaytimeout =  180;//60;
    private static long connecttimeout = 120;//40;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .connectTimeout(connecttimeout,TimeUnit.SECONDS)
            .writeTimeout(delaytimeout,TimeUnit.SECONDS)
            .readTimeout(delaytimeout,TimeUnit.SECONDS);

    private static ApiInterface apiService;
    private static Map<String, ApiIpInterface> apiServices = new HashMap<>();

    public ApiClient() {
    }

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    public static ApiInterface getApiClient() {
        if (apiService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.base_endurl)
                  //  .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
            apiService = retrofit.create(ApiInterface.class);
        }
        return apiService;
    }

    public static ApiIpInterface getIpClient(String url) {
        if (url == null)
            return null;
        ApiIpInterface apiIpInterface = apiServices.get(url);
        if (apiIpInterface == null) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())

                    .build();
            apiIpInterface = retrofit.create(ApiIpInterface.class);


            apiServices.put(url, apiIpInterface);
        }
        return apiIpInterface;
    }
    public static interface ApiInterface {

        @GET("{jspfile}")
        Call<String> onTemplateRequestForResponseLogin(@Path("jspfile") String jspfile, @QueryMap Map<String, String> query1);

    }

    public static interface ApiIpInterface {
//        @GET("aft53glg/{id}")       //i4r7eVsE      aft53glg      wdr56hf4
//        Call<TblChallenge> requestForTblChallenge(@Path("id") String stringurl);
//
//        @GET("aft53glg/{id}")
//        Call<ResponseLogin> requestForLoginResponse(@Path("id") String stringurl);
    }
}
