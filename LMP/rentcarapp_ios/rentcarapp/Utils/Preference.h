//
//  Perference.h
//  freecket
//


#import <Foundation/Foundation.h>

@interface Preference : NSObject

+(Preference*) getInstance;
-(void) putSharedPreference:(id) context :(NSString*) key  :(NSString*) value;
-(void) putSharedPreference:(id) context :(NSString*) key  WithBOOL:(BOOL) value;
-(void) putSharedPreference:(id) context :(NSString*) key  WithINT:(int) value;
-(NSString*) getSharedPreference:(id) context  :(NSString*) key  :(NSString*) defaultValue;
-(int) getSharedPreference:(id) context  :(NSString*) key  WithINT:(int) defaultValue ;
-(BOOL) getSharedPreference:(id) context  :(NSString*) key  WithBOOL:(BOOL) defaultValue ;

@end
