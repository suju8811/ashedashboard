//
//  Constants.h
//  StarRentCar
//


#import <Foundation/Foundation.h>

#define ERR_INVALID_DATA        @"Invalid Data."


extern NSString *const IS_LOGINED;
extern NSString *const PREF_COOKIE;
extern NSString *const PREF_CSRFTOKEN;

extern NSString *const PREF_USERDETAIL_COOKIE_NAME;
extern NSString *const PREF_USERDETAIL_DISPLAY_NAME;
extern NSString *const PREF_USERDETAIL_REMARK;
extern NSString *const PREF_USERDETAIL_MONTH_FLOW_BALANCE;
extern NSString *const PREF_USERDETAIL_EXTRA_FLOW_BALANCE;
extern NSString *const PREF_USERDETAIL_START_TIME;
extern NSString *const PREF_USERDETAIL_EXPIRE_TIME;
extern NSString *const PREF_USERDETAIL_DEVICE_NUM;

extern NSString *const PREF_USERDETAIL_USER_ID;
extern NSString *const PREF_USERDETAIL_USER_NAME;


extern NSString *const PREF_USERDETAIL_PLAN_ID;
extern NSString *const PREF_USERDETAIL_PLAN_NAME;
extern NSString *const PREF_USERDETAIL_PLAN_REMARK;
extern NSString *const PREF_USERDETAIL_PLAN_MONTH_FLOW;
extern NSString *const PREF_USERDETAIL_PLAN_EXTRA_FLOW;
extern NSString *const PREF_USERDETAIL_PLAN_SPEED_LIMIT;
extern NSString *const PREF_USERDETAIL_PLAN_DEVICE_NUMBER_LIMIT;
extern NSString *const PREF_USERDETAIL_PLAN_DURATION;
extern NSString *const PREF_USERDETAIL_PLAN_PRICE;
extern NSString *const PREF_USERDETAIL_PLAN_PID;

extern NSString *const PREF_SHARE_URL;

extern NSString *const PREF_REFER_INFO;
extern NSString *const PREF_HAS_TRY_FREE_PLAN;
extern NSString *const PREF_LAST_PAYMENT;

extern NSString *const PREF_LAST_PAYMENT_ID;
extern NSString *const PREF_LAST_PAYMENT_NAME;
extern NSString *const PREF_LAST_PAYMENT_PRICE;
extern NSString *const PREF_LAST_PAYMENT_PID;

static NSString *pattern_alphanumeric = @"^[a-zA-Z0-9]+$";


@interface Constants : NSObject

@end
